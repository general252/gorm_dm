module gitee.com/general252/gorm_dm

go 1.20

require (
	gitee.com/general252/dm v0.0.1
	gorm.io/gorm v1.25.12
)

require (
	github.com/golang/snappy v0.0.1 // indirect
	github.com/jinzhu/inflection v1.0.0 // indirect
	github.com/jinzhu/now v1.1.5 // indirect
	golang.org/x/text v0.14.0 // indirect
)
